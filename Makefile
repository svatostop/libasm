# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/11/18 15:03:59 by lgorilla          #+#    #+#              #
#    Updated: 2020/11/19 16:24:14 by lgorilla         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SRCS = ft_write.s ft_read.s ft_strlen.s ft_strcmp.s ft_strcpy.s ft_strdup.s

SRCS_B =  ft_atoi_base_bonus.s ft_list_size_bonus.s ft_list_push_front_bonus.s

OBJS = $(SRCS:.s=.o)

B_OBJS = $(SRCS_B:.s=.o)

NAME = libasm.a

%.o: %.s 
	@echo "\033[37;1;41m compiling libasm... \033[0m"
	nasm -f elf64 $<

all: $(NAME)
$(NAME): $(OBJS)
	@echo "\033[37;1;41m compiled. \033[0m"
	ar rcs $(NAME) $(OBJS)

clean:
	rm -rf $(OBJS) $(B_OBJS)

fclean: clean
	rm -f $(NAME)

re:	fclean all

bonus: $(OBJS) $(B_OBJS)
	@echo "\033[37;1;41m compiling bonus... \033[0m"
	ar rcs $(NAME) $(OBJS) $(B_OBJS)
	

run:
	@echo "\033[37;1;41m compiling executable... \033[0m"
	clang -Wall -Wextra -Werror main.c libasm.a
	@echo "\033[37;1;41m running... \033[0m"
	@echo ""
	@./a.out

run_b:
	@echo "\033[37;1;41m compiling executable... \033[0m"
	clang -Wall -Wextra -Werror main_bonus.c libasm.a
	@echo "\033[37;1;41m running... \033[0m"
	@echo ""
	@./a.out

.PHONY:	run all clean fclean re
