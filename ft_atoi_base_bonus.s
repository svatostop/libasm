section	.data
base:	db "0123456789abcdef", 0
base_2:	db "0123456789ABCDEF", 0

section .text
		global ft_atoi_base_bonus

ft_atoi_base_bonus:
			xor		rax, rax
			xor		rcx, rcx
			xor 	rdx, rdx
			xor		r10, r10
			xor 	r9, r9
			xor		r8, r8
			mov 	r11, base

check_base:
			mov		dl, byte[rsi + r8]
			test	dl, dl
			je		get_base
			cmp 	dl, byte[r11 + r8]
			jne 	check_base_hex
			inc 	r8
			jmp		check_base

check_base_hex:
			mov		r11, base_2
			cmp 	dl, byte[r11 + r8]
			jne 	ret_yyy
			inc		r8
			jmp 	check_base

get_base:
			mov		rsi, r8
 	
check_str:
			mov		cl, byte[rdi + r10]
			test	cl, cl
			je		return

check_chars:
			cmp 	cl, 9
			jl		check_space
			cmp 	cl, 13
			jg		check_space
			inc		r10
			jmp		check_str

check_space:
			cmp 	cl, 32
			jne		check_plus
			inc		r10
			jmp		check_str

check_plus:
			cmp 	cl, 43
			jne		check_min
			inc		r10
			jmp		check_str

check_min:
			cmp 	cl, 45
			jne 	compare
			mov 	r9, 1
			inc 	r10
			jmp		check_str

compare:
			cmp 	cl, 48
			jl		check_hex
			cmp 	cl, 57
			jg	 	check_hex

			sub 	cl, 48
			imul	rax, rsi
			add 	rax, rcx

			inc		r10
			jmp 	check_str

check_hex:
			cmp 	cl, 65
			jl		ret_yyy
			cmp 	cl, 70
			jg 		check_hex_2
			sub		cl, 65
			add		rcx, 10
			imul	rax, rsi
			add 	rax, rcx

			inc		r10
			jmp 	check_str

check_hex_2:
			cmp 	cl, 97
			jl		ret_yyy
			cmp 	cl, 102
			jg		ret_yyy
			sub		cl, 97
			add		rcx, 10
			imul	rax, rsi
			add 	rax, rcx

			inc		r10
			jmp 	check_str

return:
			cmp 	r9, 1
			je 		neg_result
			ret

neg_result:
			imul 	rax, -1
			ret

ret_yyy: 
		mov rax, 0
		ret