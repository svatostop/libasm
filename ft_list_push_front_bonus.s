section .text
		global ft_list_push_front_bonus
		extern malloc

;

ft_list_push_front_bonus:
		xor		rax, rax
		cmp 	rdi, 0
		je 		ret_z
		push 	rdi
		push 	rsi
		mov 	rdi, 16
		call	malloc
		test	rax, rax
		jz		ret_z
		pop		rsi
		pop		rdi
		mov		[rax], rsi ; помещаем в выделенный адрес нашу новую дату
		mov		rdx, [rdi] ; помещаем begin_list  
		mov		[rax + 8], rdx ; lst->next = begin_list
		mov		[rdi], rax ; begin_list = lst

ret_z:
		ret