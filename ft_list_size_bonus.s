section .text
		global ft_list_size_bonus

ft_list_size_bonus:
		xor		rax, rax

while:
		cmp		rdi, 0
		je 		return
		mov		rdi, [rdi + 8]
		inc		rax
		jmp		while

return:
		ret