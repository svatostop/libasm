section .text
		global ft_read
		extern __errno_location

ft_read:
		mov		rax, 0
		syscall
		test		rax, rax
		js		error
		ret
error:
		mov		r10, rax
		call		__errno_location
		mov		qword[rax], r10
		mov		rax, -1
		ret
