section .test
		global ft_strcmp

ft_strcmp:
		test	rdi, rdi
		jz		ret_z
		test	rsi, rsi
		jz		ret_z
		xor		rax, rax
		xor		rdx, rdx
		xor		rcx, rcx

compare:
		mov al, byte[rdi + rdx]
		cmp al, byte[rsi + rdx]
		jne return
		test al, al
		jz ret_z
		inc rdx
		jmp compare

return:
		mov cl, byte[rsi + rdx]
		sub rax, rcx
		ret

ret_z:
		xor rax, rax
		ret