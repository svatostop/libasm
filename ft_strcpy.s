section .text
		global ft_strcpy

ft_strcpy:
		test	rsi, rsi
		jz		ret_z
		test	rdi, rdi
		jz		ret_z
		xor		rax, rax
		xor		rdx, rdx

copy:
		mov		cl, [rsi + rdx]
		mov		[rdi + rdx], cl
		cmp		cl, byte 0
		je		ret_z
		inc		rdx
		jmp		copy

ret_z:
		mov		rax, rdi
		ret