section .text
		global ft_strdup
		extern ft_strlen
		extern ft_strcpy
		extern malloc

ft_strdup:
		test	rdi, rdi
		jz		ret_z
		xor		rsi, rsi
		mov		r10, rdi
		push	r10
		call	ft_strlen
		inc		rax
		mov		rdi, rax
		call	malloc
		pop		r10
		test	rax, rax
		jz		ret_z
		mov		rdi, rax
		mov		rsi, r10
		call	ft_strcpy
		ret

ret_z:
		xor		rax, rax
		ret

