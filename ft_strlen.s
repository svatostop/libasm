section .text
		global ft_strlen

ft_strlen:
		test rdi, rdi
		jz error
		xor rax, rax
while:
		cmp [rdi + rax], byte 0
		je return
		inc rax
		jmp while

error:
		xor rax, rax
		ret

return:
		ret