/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/18 15:15:36 by lgorilla          #+#    #+#             */
/*   Updated: 2020/11/19 16:27:41 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_H
# define LIBASM_H

# include <stdio.h>
# include <errno.h>
# include <stdlib.h>

size_t		ft_write(int fd, void const *ptr, size_t bytes);
size_t		ft_read(int fd, void *ptr, size_t bytes);
long		ft_strlen(char const *str);
int			ft_strcmp(char const *str1, char const *str2);
char		*ft_strcpy(char *dst, char const *src);
char		*ft_strdup(char const *s);

#endif
