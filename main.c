/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 16:23:51 by lgorilla          #+#    #+#             */
/*   Updated: 2020/11/19 16:29:45 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm.h"

int			main(void)
{
	char	c;
	int		z;
	char	*s1;
	char	*s2;

	printf("\n-------------------------------\n");
	printf("--------FT_WRITE CHECK---------\n");
	printf("-------------------------------\n");
	z = ft_write(10, "Hello\n", 6);
	printf("ERROR = %d\n", z);
	z = ft_write(1, "Hello\n", 6);
	printf("NORM = %d\n", z);
	printf("\n-------------------------------\n");
	printf("--------FT_READ CHECK----------\n");
	printf("-------------------------------\n");
	ft_read(1, &c, 3);
	ft_write(1, &c, 3);
	printf("\n-------------------------------\n");
	printf("--------FT_STRLEN CHECK--------\n");
	printf("-------------------------------\n");
	z = ft_strlen(NULL);
	printf("NULL STR = %d\n", z);
	z = ft_strlen("1234567");
	printf("NORM STR = %d\n", z);
	printf("\n-------------------------------\n");
	printf("--------FT_STRDUP CHECK--------\n");
	printf("-------------------------------\n");
	s1 = ft_strdup("Hey, fistr str\n");
	s2 = ft_strdup("Hey, sec str\n");
	printf("First str:\n %sSec str: \n %s\n", s1, s2);
	printf("\n-------------------------------\n");
	printf("--------FT_STRCPY CHECK--------\n");
	printf("-------------------------------\n");
	ft_strcpy(s1, s2);
	printf("First str:\n %sSec str: \n %s\n", s1, s2);
	printf("\n-------------------------------\n");
	printf("--------FT_STRCMP CHECK--------\n");
	printf("-------------------------------\n");
	z = ft_strcmp("1235", "12");
	printf("%d\n", z);
	z = ft_strcmp(s1, s2);
	printf("%d\n", z);
	return (0);
}
