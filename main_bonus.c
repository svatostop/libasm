/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lgorilla <lgorilla@student.21-school.ru>   +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/11/19 16:23:51 by lgorilla          #+#    #+#             */
/*   Updated: 2020/11/19 16:31:59 by lgorilla         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libasm_bonus.h"
#include "libasm.h"

t_list		*ft_lstnew(void *data)
{
	t_list *tmp;

	tmp = (t_list*)malloc(sizeof(t_list));
	if (tmp != NULL)
	{
		tmp->data = data;
		tmp->next = NULL;
	}
	return (tmp);
}

void		ft_lstadd_back(t_list **lst, t_list *new)
{
	t_list *tmp;

	if (!lst || !new)
		return ;
	tmp = *lst;
	if (tmp)
	{
		while (tmp->next != NULL)
			tmp = tmp->next;
		tmp->next = new;
	}
	else
		*lst = new;
}

void		write_lst(t_list *list)
{
	while (list != NULL)
	{
		printf("%s\n", list->data);
		list = list->next;
	}
}

int			main(void)
{
	int		z;
	t_list	list;
	t_list	list_sec;
	t_list	list_third;
	t_list	*lst;

	printf("\n-----------------------------------\n");
	printf("--------FT_ATOI_BASE CHECK---------\n");
	printf("-----------------------------------\n");
	z = ft_atoi_base_bonus("\t 12334", "0123456789");
	printf("DEC = %d\n", z);
	z = ft_atoi_base_bonus("\f\n\r -12334", "0123456789");
	printf("NEG_DEC = %d\n", z);
	z = ft_atoi_base_bonus("\f\n\r 2a", "0123456789abcdef");
	printf("HEX = %d\n", z);
	z = ft_atoi_base_bonus("\f\n\r 564", "01234567");
	printf("OCT = %d\n", z);
	z = ft_atoi_base_bonus("\f\n\r +1101011101", "01");
	printf("BIN = %d\n", z);
	z = ft_atoi_base_bonus("\t -2147483648", "0123456789");
	printf("DEC = %d\n", z);
	z = ft_atoi_base_bonus("    2147483647", "0123456789");
	printf("DEC = %d\n", z);
	z = ft_atoi_base_bonus("\f\n\r 124", "10");
	printf("ERROR = %d\n", z);
	printf("\n-----------------------------------\n");
	printf("----------FT_LIST_SIZE CHECK-------\n");
	printf("-----------------------------------\n");
	list.data = ft_strdup("12345");
	list.next = &list_sec;
	list_sec.data = ft_strdup("6789");
	list_sec.next = &list_third;
	list_third.data = ft_strdup("09987");
	list_third.next = NULL;
	printf("\n------\n Print List \n------\n");
	write_lst(&list);
	z = ft_list_size_bonus(&list);
	printf("SIZE = %d\n", z);
	printf("\n-----------------------------------\n");
	printf("-----FT_LIST_PUSH_FRONT CHECK------\n");
	printf("-----------------------------------\n");
	lst = 0;
	lst = ft_lstnew(ft_strdup("pa,aa"));
	ft_lstadd_back(&lst, ft_lstnew(ft_strdup("woooow")));
	ft_list_push_front_bonus(&lst, "heyhow");
	z = ft_list_size_bonus(lst);
	printf("\n------\n Print List \n------\n");
	write_lst(lst);
	printf("SIZE = %d\n", z);
	return (0);
}
